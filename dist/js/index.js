let pages = [];
const otherPages = document.querySelectorAll('nav div a');

otherPages.forEach(el => {
    pages.push(el.hash.substr(1));
})
const allSections = document.querySelectorAll('section');

window.onhashchange = () => {
    init();
}

window.onload = () => {
    init();
}

const init = () => {
    const page = gethash();
    if(page) {
        handleChange(page);
    }
}

const gethash = () => {
    const urlExtension = window.location.hash.slice(1);
    let page;
    
    if (!urlExtension || !pages.includes(urlExtension)) {
        window.location.hash = '#home';

    } else {
        page  = urlExtension;
    }
    return page;
}

const handleChange = (page) => {
    let changeRequired = true;
    allSections.forEach(section => {
        if(section.classList.contains('visible-section') && section.classList.contains(page)) {
            changeRequired = false;
        }
    })
    if(changeRequired) {
        clearAlert();
        hideWebSite();
    
        setTimeout(() => {
            hideAllSections();
            showHashSection(page);
            
        }, 200)
        setTimeout(function() {
            showWebSite();
            if(page === "home") {
                calcMaxColHeight(columnNumber)
                insertTestimonials();
                setTestimonialHeight();
                try {
                    initCarousel();
                } catch(e) {
                }
            }
        }, 450)
    }
}

const hideAllSections = () => {
    allSections.forEach(section => {
        if(section.classList.contains('visible-section')) {
            section.classList.remove("visible-section");
        }  
    })
}

const showHashSection = (page) => {
    const section = document.querySelector(`.${page}`);
    section.classList.add("visible-section");
}

const hideWebSite = () => {
    const webSite = document.querySelector('.web-body');
    webSite.classList.add('hidden')
}


const showWebSite = () => {
    const webSite = document.querySelector('.web-body');
    webSite.classList.remove('hidden')
}

/*
    NAV AND HAMBURGER
*/

const hamburger = document.querySelector('.hamburger');
const webTitle = document.querySelector('.web-title')
const cross = document.querySelector('.cross');
const header = document.querySelector('header')
const navBar = document.querySelector('nav');
const navItem = document.querySelectorAll('nav div')
const lines = document.querySelectorAll('nav div hr')
const mq = window.matchMedia( "(min-width: 1270px)" );

const flattenNavItems = () => {
    navBar.classList.add('flatten');
    navBar.style.height = '0px';
}

const setNavBarHeight = () => {
    const navBarHeight = navItem[0].scrollHeight * navItem.length;
    navBar.style.height = navBarHeight + 'px';
}

const openNavItems = () => {
    navBar.classList.remove('flatten');
    setNavBarHeight();
}

const openHamburger = () => {
    hamburger.classList.add("hidden", "no-display");
    cross.classList.remove("hidden", "no-display");
    openNavItems();
}

const closeHamburger = () => {
    cross.classList.add("hidden", "no-display");
    hamburger.classList.remove("hidden", "no-display");
    flattenNavItems();
}
hamburger.addEventListener('click', () => {
    openHamburger();
});

cross.addEventListener('click', () => {
    closeHamburger();
});

const widthChange = (mq) => {
    if (mq.matches) {
        navBar.classList.remove("flatten");
        navBar.style.height = 'unset';
        header.classList.remove('navigation-mobile')
        navBar.classList.remove('navigation-mobile');
        hamburger.classList.add("hidden","no-display");
        cross.classList.add("hidden", "no-display");
        lines.forEach(e => {
            e.classList.remove('hidden');
        })
        navBar.classList.remove('collapsible');
        navItem.forEach(item => {
            item.removeEventListener('click', closeHamburger);
        })
    } else {
        header.classList.add('navigation-mobile')
        navBar.classList.add('navigation-mobile');
        hamburger.classList.remove("hidden", "no-display");
        cross.classList.add("hidden", "no-display");
        lines.forEach(e => {
            e.classList.add('hidden')
        })
        navItem.forEach(item => {
            item.addEventListener('click', closeHamburger);
        })
        flattenNavItems();
        setTimeout(function() {
            navBar.classList.add('collapsible');
        }, 200);
        
    }
}
widthChange(mq);
mq.addEventListener("change", widthChange);


/* NEWS */

const newsItems = document.querySelectorAll('.news-card-wrapper');
const newsContainer = document.querySelector('.news-container');
const desktopNewsSize = window.matchMedia("(min-width: 600px)");
let columnNumber = 3;

const calcColHeight = (maxHeight, columnNumber, currentColumn) => {
    let currentHeight = 0;
    for(let j = currentColumn; j < newsItems.length; j+= columnNumber) {
        currentHeight += newsItems[j].scrollHeight;
    }
    return(currentHeight > maxHeight ? currentHeight : maxHeight);
}

const calcMaxColHeight = (columnNumber) => {
    let maxHeight = 0;
    for(let i = 0; i < columnNumber; i++) {
        maxHeight = calcColHeight(maxHeight, columnNumber, i);
    }
    const height = maxHeight + 20;
    if(desktopNewsSize.matches) {
        newsContainer.style.height = `${height}px`;
    }
}

let maxColHeight = 0;

const newsWidthChange = desktopNewsSize => {
    calcMaxColHeight(columnNumber)
    if(!desktopNewsSize.matches) {
        newsContainer.style.height = `auto`;
    }
}
window.addEventListener('resize', () => {
    newsWidthChange(desktopNewsSize)
    if(gethash() === "home" && document.querySelector('.carousel-gallery') != null)
        setTestimonialHeight()
})

/* TESTIMONIALS */

const setTestimonialHeight = () => {
    const testimonalContainer = document.querySelectorAll('.testimonial')
    const gallery = document.querySelector('.carousel-gallery');
    let maxHeight = 0;
    for(let i= 0; i < testimonalContainer.length; i++) {
        testimonalContainer[i].style.height = "unset";
        maxHeight = maxHeight > testimonalContainer[i].scrollHeight ? maxHeight : testimonalContainer[i].scrollHeight;
    }
    gallery.style.height = `${maxHeight}px`;
    for(let i= 0; i < testimonalContainer.length; i++) {
        testimonalContainer[i].style.height = "100%";
    }
}

const testimonials = 
    `
        <div class="carousel-gallery" data-flickity='{ "setGallerySize": false }'>
        <div class="gallery-cell">
            <div class="testimonial">
                <div class="review">I have had the pleasure of working with Joshua for a good number of years, and he is a very adept and extremely sensitive musician. In my opinion, his greatest assets are his capacity for listening, and offering creative ideas as part of a group. These, along with his unwavering attention to detail, make him a truly rewarding musician to work with.</div>
                <div class="reviewer">Alex Howgego, Pianist</div>
            </div>
        </div>
        <div class="gallery-cell">
            <div class="testimonial">
                <div class="review">Joshua has actively worked with my piano and piano accompaniment compositions in both the recording studio and in concert. It has been a privilege to have him perform my music. He comes to the piano keyboard with enthusiasm often after many hours of rehearsal to unfold the page before him to produce enlivened performances. It is a pleasure to work with such an accomplished musician.</div>
                <div class="reviewer">Richard Pond, Composer</div>
            </div>
        </div>
        <div class="gallery-cell">
            <div class="testimonial">
                <div class="review">Having started a church choir together nearly a year ago, Joshua has proved to be a great colleague to work with. He has a real understanding of the sacred music repertoire. He is a sensitive accompanist and always prepared to listen to advice. From being originally only a piano keyboard player, Joshua has taken the trouble to hone his skills on the organ. He has a real feel for and knowledge of music for Christian liturgy and a good knowledge of the repertoire of sacred music. He is reliable, punctual and organised. He has a good sense of humour and is always appreciative of others’ achievements.</div>
                <div class="reviewer">Tom Merry, Choirmaster, All Saints' Bisley</div>
            </div>
        </div>
        <div class="gallery-cell">
            <div class="testimonial">
                <div class="review">Joshua has been the accompanist for my choir, Philomusica of Gloucestershire and Worcestershire, for 15 months. I have found him to be totally reliable and more than willing to put in extra rehearsal time in order to play demanding scores to a high standard, whether on piano or organ. He is punctual, polite and unfailingly cheerful and is most rewarding to work with. He has also played violin solos for us in several concerts. He serves on the Music Selection committee and the Soloists’ selection committee and always makes useful contributions to both.</div>
                <div class="reviewer">Linda Parsons, Conductor, Philomusica</div>
            </div>
        </div>
        <div class="gallery-cell">
            <div class="testimonial">
                <div class="review">Joshua is always sensitive and responsive in his accompanying and I feel confident and at ease during our performances together. He sight-reads to a high standard, but always works hard to ensure he knows the music thoroughly before a performance and is a pleasure to work with.</div>
                <div class="reviewer">Phillippa Lay, Soprano</div>
            </div>
        </div>
        <div class="gallery-cell">
            <div class="testimonial">
                <div class="review">Joshua taught piano to my children, aged 5 and 7. They looked forward to the lessons and learned a lot from him. The only reason we stopped is because he moved away, otherwise we would certainly have continued. I would recommend him highly – he’s very friendly, professional, dedicated and fun and also an excellent musician. Couldn’t ask for more.</div>
                <div class="reviewer">Mel & John</div>
            </div>
        </div>
        </div>
    `

const testimonialsHomePage = document.querySelector('.testimonials-home-page')

const insertTestimonials = () => {
    if(document.querySelectorAll('.carousel-gallery').length === 0)
        testimonialsHomePage.insertAdjacentHTML("afterend", testimonials);
}

const initCarousel = () => {
    var elem = document.querySelector('.carousel-gallery');
    var flkty = new Flickity( elem, {
    // options
    cellAlign: 'left',
    contain: true,
    setGallerySize: false
    });
}

/*
    GALLERY
*/
const focusImageContainer = document.querySelector('.focus-image-background');
const focusImage = focusImageContainer.querySelector('img');


const galleryImages = document.querySelectorAll('.gallery-image img');
galleryImages.forEach(e => {
    e.addEventListener('click', event => {
        const altTag = event.target.alt;
        const imageSource = event.target.src;
        focusImage.src = imageSource;
        focusImage.alt = altTag;
        focusImageContainer.style.visibility = 'visible';
        focusImageContainer.style.opacity = 1;
        const body = document.querySelector('body');
        body.scroll = "no";
        body.style.overflow = "hidden";
        focusImageContainer.addEventListener('click', () => {
            focusImageContainer.style.visibility = 'hidden';
            focusImageContainer.style.opacity = 0;
            body.scroll = "yes";
            body.style.overflow = "visible";
        })
    })
})

/* RECORDING */

const youtube = document.querySelectorAll( ".youtube" );

for (var i = 0; i < youtube.length; i++) {
    const altTag = youtube[i].title;
    const source = "https://img.youtube.com/vi/"+ youtube[i].dataset.embed +"/sddefault.jpg";
    // Load the image asynchronously
    const image = new Image();
    image.src = source;
    image.alt = altTag;
    image.addEventListener( "load", function() {
        youtube[i].appendChild(image);
    }(i));
    youtube[i].addEventListener( "click", function() {
        const altTitle = this.title;
        const iframe = document.createElement("iframe");
        iframe.setAttribute("frameborder", "0");
        iframe.setAttribute("allowfullscreen", "");
        iframe.setAttribute("src", "https://www.youtube.com/embed/"+ this.dataset.embed +"?rel=0&showinfo=0&autoplay=1" );
        iframe.setAttribute("title", altTitle)
        this.innerHTML = "";
        this.appendChild(iframe);
    });
}

/*
    CONTACT
*/

function clearForm(name, email, message) {
    name.value = "";
    email.value = "";
    message.value = "";
}

function feedbackToUser(result) {
    if(result === "success") {
        const successMessage = document.querySelector('.alert-success');
        successMessage.classList.remove('transparent');
    } else if (result === "fail") {
        const failMessage = document.querySelector('.alert-fail');
        failMessage.classList.remove('transparent');
    } else if (result === "no-captcha") {
        const failMessage = document.querySelector('.alert-warn');
        failMessage.classList.remove('transparent');
    }
    setTimeout(clearAlert, 5000);
}

function clearAlert() {
    const alerts = document.querySelectorAll('.alert');
    alerts.forEach(alert => {
        if(!alert.classList.contains('transparent')) {
            alert.classList.add('transparent');
        }
    })
}

function processForm(e) {
    e.preventDefault();
    clearAlert();
    var URL = "https://099u15s19a.execute-api.eu-west-1.amazonaws.com/prod/contact-us";

    const name = document.querySelector('input[id="name"]');
    const email = document.querySelector('input[id="email"]');
    var message = document.querySelector('textarea[id="message"]');
    var data = {
       name : name.value,
       email : email.value,
       message : message.value,
       captchaResponse: grecaptcha.getResponse()
    };

    if(data.captchaResponse === "" || data.captchaResponse === null) {
        feedbackToUser("no-captcha");
        return;
    }

    var xhr = new XMLHttpRequest();
    xhr.open("POST", URL, true);
    xhr.setRequestHeader('Content-Type', 'application/json; charset=UTF-8');

    // send the collected data as JSON
    xhr.send(JSON.stringify(data));

    xhr.onload = function () {
        if(xhr.status === 200) {
            feedbackToUser("success");
        } else {
            feedbackToUser("fail");
        }
    };

    xhr.onerror = function() {
        feedbackToUser("fail");
    }

    clearForm(name, email, message);
     
  }
